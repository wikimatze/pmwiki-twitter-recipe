<?php if (!defined('PmWiki')) exit();

$RecipeInfo['twitter']['Version']='2011-04-20';
$HTMLHeaderFmt[] = "<link rel='stylesheet' href='\$PubDirUrl/css/twitter.css' type='text/css' />";

// Twitter-messages default config
SDV($twitter_header, 'Twitter Updates');
SDV($twitter_account, 'wikimatze');
SDV($twitter_account_follow_message, 'Follow me');
SDV($twitter_messages, 3);


// Twitter-pictures default config
SDV($twitter_picture_account, 'wikimatze');             # setting up the twitter account which you can see
SDV($twitter_picture_theme, "standard");                # in 'standard' the images will be displayed vertical. The other mode is 'grid_vertical'!
SDV($twitter_picture_background_color, "e2dfdf");       # hex-colors for background
SDV($twitter_picture_border_color, "E2E2E2");           # hex-colors for border
SDV($twitter_picture_link_color, "81b6da");             # hex-colors for links
SDV($twitter_picture_tweet_background_color, "ffffff"); # hex-colors for the background of simple tweets
SDV($twitter_picture_tweet_text_color, "140f0f");       # hex-colors for the text of the tweets
SDV($twitter_picture_width, "250");                     # display the size of twitter images
SDV($twitter_picture_count, 20);                        # [1,2,...,20]
SDV($twitter_picture_timestamp, "1");                   # display the timestamp when the picture was uploaded


$display_messages  = "<h4 class=\"sidebar-title\" style=\"display: black;\">$twitter_header</h4>\n";
$display_messages .= "<ul id=\"twitter_update_list\"></ul>\n";
$display_messages .= "<a href=\"http://twitter.com/$twitter_account\" id=\"twitter-link\" style=\"display:block;text-align:right;\">$twitter_account_follow_message</a>\n";
$display_messages .= "<script type=\"text/javascript\" src='$PubDirUrl/twitter/twitter.js'></script>\n";

$display_messages .= "<script type=\"text/javascript\" src=\"http://twitter.com/statuses/user_timeline/$twitter_account.json?callback=twitterCallback2&amp;count=$twitter_messages\"></script>\n";

$display_picture   = "<script type=\"text/javascript\" src=\"http://widgets.twitpic.com/j/2/widget.js?username=$twitter_picture_account&";
$display_picture  .= "colorbg=$twitter_picture_background_color&";
$display_picture  .= "colorborder=$twitter_picture_border_color&";
$display_picture  .= "colorlink=$twitter_picture_link_color&";
$display_picture  .= "colortweetbg=$twitter_picture_tweet_background_color&";
$display_picture  .= "colortweets=$twitter_picture_tweet_text_color&";
$display_picture  .= "theme=$twitter_picture_theme&";
$display_picture  .= "width=$twitter_picture_width&";
$display_picture  .= "count=$twitter_picture_count&";
$display_picture  .= "timestamp=$twitter_picture_timestamp\"></script>";

Markup("twittermessages", "block","/\(:twittermessages:\)/", "$display_messages");
Markup("twitterpictures", "block","/\(:twitterpictures:\)/", "$display_picture");
