# twitter

Display twitter-messages and twitter-pictures from a any member on twitter in your page.

![Picture of the recipe](http://www.pmwiki.org/pmwiki/uploads/Cookbook/twitter.png)

## Questions answered by this recipe

How can I easily display **twitter messages** and **twitpic images** on my page?


## Description

The *twitter.php* recipe provides two simple markups

- `(:twittermessages:)` for displaying the last *entries*
- `(:twitterpictures:)` for displaying the last *pictures*

Variables for configuring the display of **twitter-messages**:

- `$twitter_header` the header, where you twitter-messages will appear
- `$twitter_account` the person, from which you will display the -tweets-
- `$twitter_account_follow_message` the message for following the displayed twitter-account
- `$twitter_messages` how many messages should be displayed

Variables for configuring the display of **twitter-pictures**:

- `$twitter_picture_account` setting up the twitter account which you can see
- `$twitter_picture_theme` in 'standard' the images will be displayed vertical. The other mode is
  `grid_vertical`
- `$twitter_picture_background_color` hex-colors for background
- `$twitter_picture_border_color` hex-colors for border
- `$twitter_picture_link_color` hex-colors for links
- `$twitter_picture_tweet_background_color` hex-colors for the background of simple tweets
- `$twitter_picture_link_color` hex-colors for links
- `$twitter_picture_tweet_text_color` hex-colors for the text of the tweets
- `$twitter_picture_width` display the size of twitter images
- `twitter_picture_count` *[1,2,...,20]*
- `twitter_picture_timestamp` display the timestamps when the picture was uploaded

To configure the colors for the color-variables for twitter-pictures you have to use hexadecimal digits.

Just place the variables **before including** the `twitter.php` in your *config.php*.

Here is an example-configuration:

    // Twitter-messages
    $twitter_header = "Twitter Neuigkeiten";
    $twitter_account = "Helex";
    $twitter_account_follow_message = "Folge mir";
    $twitter_messages = 3;


    // Twitter-pictures
    $twitter_picture_account = 'wikimatze';
    $twitter_picture_theme = "standard";
    $twitter_picture_background_color = "e2dfdf";
    $twitter_picture_border_color = "E2E2E2";
    $twitter_picture_link_color = "81b6da";
    $twitter_picture_tweet_background_color = "ffffff";
    $twitter_picture_tweet_text_color = "140f0f";
    $twitter_picture_width = "250";
    $twitter_picture_count = 20;
    $twitter_picture_timestamp = "1";

    include_once("$FarmD/cookbook/twitter.php");

It is possible to change the layout of the displaying twitter messages with the following variables:

    #twittermessages {...}          // the div id for the twitter messages
    #twittermessages li {...}       // styling of one twitter message as a list
    .twittermessages_status {...}   // styling of the font of the messages
    .twittermessages_status a {...} // decoration of the links
    .twittermessages_relativeTime   // styling of the posted date of a message

To change the layout please go to `pub/css/twitter.css`.


## Installation

- download from [source][source] or via git `https://bitbucket.org/wikimatze/pmwiki-twitter-recipe.git`
- make the following copy-tasks:
    - put `twitter.php` file to your cookbook directory (`cp pmwiki-twitter-recipe/cookbook/twitter/twitter.php cookbook/`)
    - put `twitter.css` file to your public directory (normally *pub/*) (`cp pmwiki-twitter-recipe/pub/css/twitter.css pub/css/`)
    - put `twitter`-directory in your public directory (normally *pub/*) (`cp -R  pmwiki-twitter-recipe/pub/twitter pub/`)
- add to *local/config.php* `include_once("$FarmD/cookbook/twitter.php");` and don't forget to put
  the variables suggested above in Description section before including the *twitter.php* if not,
  then the default values of the SDVs will be loaded
- config the *twitter.php*-file with your account-name (see Description section)


## Notes

Twitter works quite well. So far I've encountered the following problems:

- if you want to display twitter messages (or twitter pictures) in more than one place, than nothing
  happened because the twitter-API prevents overhead
- it may happen that you will see nothing on your page if you have to much traffic
- to style the displayed messages, just take a look in the *twitter.js* where I inserted some CSS


## See Also

- [TwitterPost][TwitterPost]) - easily post updates to your Twitter account via PHP functions


## Comments/Feedback

See [Twitter-Talk][talk] - your comments are welcome there!


## Contact

Feature request, bugs, questions, etc. can be send to <matthias.guenther@wikimatze.de>. If you enjoy
the script please leave your comment under [Twitter Users][Twitter Users].


## License

This software is licensed under the [MIT license][mit].

© 2011-2017 Matthias Guenther <matthias@wikimatze.de>.

[Twitter Users]: http://www.pmwiki.org/wiki/Cookbook/Twitter-Users
[TwitterPost]: http://www.pmwiki.org/wiki/Cookbook/TwitterPost
[mit]: http://en.wikipedia.org/wiki/MIT_License
[source]: https://bitbucket.org/wikimatze/pmwiki-twitter-recipe/overview
[talk]: http://www.pmwiki.org/wiki/Cookbook/Twitter-Talk
